sudo apt-get update

# install chrony
sudo apt-get install -y chrony

# install guacd dependencies
sudo apt-get install -y make gcc g++ libcairo2-dev libjpeg62-turbo-dev libpng-dev libtool-bin uuid-dev libossp-uuid-dev libavcodec-dev libavutil-dev libswscale-dev libpango1.0-dev libssh2-1-dev libvncserver-dev libtelnet-dev libssl-dev libvorbis-dev libwebp-dev libpulse-dev libwebsockets-dev freerdp2-dev

# install guacd server to /opt
sudo wget https://dlcdn.apache.org/guacamole/1.4.0/source/guacamole-server-1.4.0.tar.gz -P /tmp/
sudo tar -xzf /tmp/guacamole-server-1.4.0.tar.gz -C /opt
sudo rm /tmp/guacamole-server-1.4.0.tar.gz 
cd /opt/guacamole-server-1.4.0
sudo /opt/guacamole-server-1.4.0/configure --with-init-dir=/etc/init.d --enable-allow-freerdp-snapshots
sudo make
sudo make install
sudo ldconfig
sudo sed -i 's/getpid > \/dev\/null || $exec -p "$pidfile"/getpid > \/dev\/null || $exec -p "$pidfile" -b 0.0.0.0/' /etc/init.d/guacd 	
sudo systemctl daemon-reload
sudo systemctl start guacd
sudo systemctl enable guacd
